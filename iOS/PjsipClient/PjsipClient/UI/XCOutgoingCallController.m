//
//  XCOutgoingCallController.m
//  simpleVoIP
//
//  Created by Igor Nazarov on 12/27/16.
//  Copyright © 2016 Xianwen Chen. All rights reserved.
//

#import "XCOutgoingCallController.h"
#import "XCPjsua.h"
#import "XCIncomingCallController.h"
#import "XCActiveCallController.h"
#import "Utils.h"

@interface XCOutgoingCallController () <UITextFieldDelegate, XCPjsuaListener>

@property (nonatomic, weak) IBOutlet UITextField* textField;

@end

@implementation XCOutgoingCallController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
    
    [[XCPjsua sharedXCPjsua] addListener:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[XCPjsua sharedXCPjsua] removeListener:self];
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)dial:(id)sender
{
    if (self.textField.text.length > 0)
    {
        NSString* username = self.textField.text;
        NSString* server = [Utils getServer];
        NSString* port = [Utils getPort];
        
        NSString* destUri = [NSString stringWithFormat:@"sip:%@@%@:%@", username, server, port];
        
        [[XCPjsua sharedXCPjsua] makeCallTo:(char*)destUri.UTF8String];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - XCPjsuaListener

- (void)onCallState:(CallState)callState callId:(int)callId
{
    if (callState == CallStateIncoming)
    {
        [self handleIncomingCall:callId];
    }
    else if (callState == CallStateConfirmed)
    {
        [self callConfirmed:callId];
    }
}

#pragma mark - Utils

- (void)handleIncomingCall:(int)callId
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NSString* screenID = NSStringFromClass([XCIncomingCallController class]);
    
    XCIncomingCallController* incomingCallScreen = [storyboard instantiateViewControllerWithIdentifier:screenID];
    incomingCallScreen.callID = callId;
    [self.navigationController pushViewController:incomingCallScreen animated:YES];
}

- (void)callConfirmed:(int)callId
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NSString* screenID = NSStringFromClass([XCActiveCallController class]);
    
    XCActiveCallController* activeCallScreen = [storyboard instantiateViewControllerWithIdentifier:screenID];
    activeCallScreen.callID = callId;
    [self.navigationController pushViewController:activeCallScreen animated:YES];
}

@end
