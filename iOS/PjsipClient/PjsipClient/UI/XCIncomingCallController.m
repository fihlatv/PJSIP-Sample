//
//  XCIncomingCallController.m
//  simpleVoIP
//
//  Created by Igor Nazarov on 12/27/16.
//  Copyright © 2016 Xianwen Chen. All rights reserved.
//

#import "XCIncomingCallController.h"
#import "XCPjsua.h"
#import "XCActiveCallController.h"

@interface XCIncomingCallController () <XCPjsuaListener>

@end

@implementation XCIncomingCallController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    [[XCPjsua sharedXCPjsua] addListener:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[XCPjsua sharedXCPjsua] removeListener:self];
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)decline:(id)sender
{
    [[XCPjsua sharedXCPjsua] endCall];
}

- (IBAction)answer:(id)sender
{
    [[XCPjsua sharedXCPjsua] answerCall:self.callID];
}

#pragma mark - XCPjsuaListener

- (void)onCallState:(CallState)callState callId:(int)callId
{
    if (callState == CallStateConfirmed)
    {
        [self callConfirmed:callId];
    }
    else if (callState == CallStateDisconnected)
    {
        [self callDeclined:callId];
    }
}

#pragma mark - Utils

- (void)callConfirmed:(int)callID
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NSString* screenID = NSStringFromClass([XCActiveCallController class]);
    
    XCActiveCallController* activeCallScreen = [storyboard instantiateViewControllerWithIdentifier:screenID];
    activeCallScreen.callID = callID;
    [self.navigationController pushViewController:activeCallScreen animated:YES];
}

- (void)callDeclined:(int)callID
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}

@end
