/*
 * Copyright (C) 2014 Xianwen Chen <xianwen@xianwenchen.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#import "XCPjsua.h"
#import "pjsua.h"

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Utils.h"

#define THIS_FILE "XCPjsua.c"

const size_t MAX_SIP_ID_LENGTH = 200;
const size_t MAX_SIP_REG_URI_LENGTH = 200;

static void on_reg_state2(pjsua_acc_id acc_id, pjsua_reg_info *info);
static void on_incoming_call(pjsua_acc_id acc_id, pjsua_call_id call_id, pjsip_rx_data *rdata);
static void on_call_state(pjsua_call_id call_id, pjsip_event *e);
static void on_call_media_state(pjsua_call_id call_id);
static void on_transport_state(pjsip_transport *tp, pjsip_transport_state state, const pjsip_transport_state_info *info);

@interface XCPjsua ()
{
    pjsua_acc_id _acc_id;
    
    pjsua_transport_id transport_id;
    pjsua_transport_id transport_id_ipv6;
    
    pjsua_call_id active_call_id;
    
    NSMutableArray* listeners;
}

@end

@implementation XCPjsua

+ (XCPjsua *)sharedXCPjsua
{
    static XCPjsua *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[XCPjsua alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        listeners = [NSMutableArray array];
    }
    
    return self;
}

#pragma mark - Listeners

- (void)addListener:(id<XCPjsuaListener>)listener
{
    [listeners addObject:listener];
}

- (void)removeListener:(id<XCPjsuaListener>)listener
{
    __block id listenerToRemove = nil;
    
    [listeners enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        id<XCPjsuaListener> lis = (id<XCPjsuaListener>)obj;
        if (lis == listener)
        {
            listenerToRemove = lis;
        }
    }];
    
    if (listenerToRemove)
    {
        [listeners removeObject:listenerToRemove];
    }
}

#pragma mark - Public interface

- (void)startPjsip
{
    pj_status_t status;
    
    pj_log_set_level(5);
    
    // Create pjsua first
    status = pjsua_create();
    if (status != PJ_SUCCESS)
    {
        NSLog(@"Failed to start Pjsip");
    }
    
    // Init pjsua
    pjsua_config cfg;
    pjsua_config_default (&cfg);
//    cfg.stun_try_ipv6 = PJ_TRUE;
    
    cfg.cb.on_incoming_call = &on_incoming_call;
    cfg.cb.on_call_media_state = &on_call_media_state;
    cfg.cb.on_call_state = &on_call_state;
    cfg.cb.on_reg_state2 = &on_reg_state2;
    cfg.cb.on_transport_state = &on_transport_state;
    
    // Init the logging config structure
    pjsua_logging_config log_cfg;
    pjsua_logging_config_default(&log_cfg);
    log_cfg.console_level = 4;
    
    status = pjsua_init(&cfg, &log_cfg, NULL);
    if (status != PJ_SUCCESS)
    {
        NSLog(@"Failed to init Pjsua");
    }
    
    unsigned port = [[Utils getPort] intValue];
    
    // Add UDP transport.
    {
        // Init transport config structure
        pjsua_transport_config cfg;
        pjsua_transport_config_default(&cfg);
        cfg.port = port;

        // Add UDP transport.
        status = pjsua_transport_create(PJSIP_TRANSPORT_UDP, &cfg, &transport_id);
        if (status != PJ_SUCCESS)
        {
            NSLog(@"Failed to create UDP transport");
        }
    }
    
    // Add UDP6 transport.
    {
        // Init transport config structure
        pjsua_transport_config cfg;
        pjsua_transport_config_default(&cfg);
        cfg.port = port;
        
        // Add UDP6 transport.
        status = pjsua_transport_create(PJSIP_TRANSPORT_UDP6, &cfg, &transport_id_ipv6);
        if (status != PJ_SUCCESS)
        {
            NSLog(@"Failed to create UDP6 transport");
        }
    }
    
    // Initialization is done, now start pjsua
    status = pjsua_start();
    if (status != PJ_SUCCESS)
    {
        NSLog(@"Failed to start Pjsua");
    }
}

- (void)registerOnServer:(char *)sipDomain withUserName:(char *)sipUser andPassword:(char *)password
{
    char* port = (char*)[Utils getPort].UTF8String;
    
    pj_status_t status;
    
    pjsua_acc_config cfg;
    pjsua_acc_config_default(&cfg);
    
    // Account ID
    char sipId[MAX_SIP_ID_LENGTH];
    sprintf(sipId, "sip:%s@%s:%s", sipUser, sipDomain, port);
    cfg.id = pj_str(sipId);
    
    // Reg URI
    char regUri[MAX_SIP_REG_URI_LENGTH];
    sprintf(regUri, "sip:%s:%s", sipDomain, port);
    cfg.reg_uri = pj_str(regUri);
    
    // Account cred info
    cfg.cred_count = 1;
    cfg.cred_info[0].scheme = pj_str("Digest");
    cfg.cred_info[0].realm = pj_str("*");
    cfg.cred_info[0].username = pj_str(sipUser);
    cfg.cred_info[0].data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
    cfg.cred_info[0].data = pj_str(password);
    
    cfg.proxy_cnt = 0;
    cfg.reg_use_proxy = 0;
    
    cfg.transport_id = [self isIPv6] ? transport_id_ipv6 : transport_id;
    cfg.ipv6_media_use = [self isIPv6] ? PJSUA_IPV6_ENABLED : PJSUA_IPV6_DISABLED;
    
    cfg.ip_change_cfg.shutdown_tp = PJ_TRUE;
    cfg.allow_contact_rewrite = PJ_TRUE;
    cfg.allow_via_rewrite = PJ_TRUE;
    cfg.contact_rewrite_method = PJSUA_CONTACT_REWRITE_ALWAYS_UPDATE;
    cfg.ip_change_cfg.reinvite_flags = 0;
    
    cfg.ice_cfg_use = PJSUA_ICE_CONFIG_USE_CUSTOM;
    cfg.ice_cfg.enable_ice = [self isIPv6] ? YES : NO;
    cfg.ice_cfg.enable_ice = YES;
    
//    cfg.nat64_opt = PJSUA_NAT64_ENABLED;
    
    pjsip_cfg_t* sip_cfg = pjsip_cfg();
    sip_cfg->endpt.resolve_hostname_to_get_interface = PJ_TRUE;
    
    status = pjsua_acc_add(&cfg, PJ_TRUE, &_acc_id);
    if (status != PJ_SUCCESS)
    {
        NSLog(@"Error adding account");
    }
}

- (void)makeCallTo:(char*)destUri
{
    pj_status_t status;
    char* key = "1";
    pj_str_t uri = pj_str(destUri);
    
    status = pjsua_verify_sip_url(destUri);
    
    pjsua_msg_data msg_data;
    pjsua_msg_data_init(&msg_data);
    
    status = pjsua_call_make_call(_acc_id, &uri, 0, (void*)key, &msg_data, &active_call_id);
    if (status != PJ_SUCCESS)
    {
        NSLog(@"Failed to make a call");
    }
}

- (void)answerCall:(int)callID
{
    pjsua_call_answer(callID, 200, NULL, NULL);
}

- (void)hangupCall:(int)callID
{
    [self endCall];
    pjsua_call_hangup(callID, 0, NULL, NULL);
}

- (void)endCall
{
    pjsua_call_hangup_all();
}

- (void)changeIp
{
    unsigned call_count = pjsua_call_get_count();
    if (call_count)
    {
        pjsua_acc_config acc_cfg;
        pj_pool_t *tmp_pool = pjsua_pool_create("tmp-pjsua", 1000, 1000);
        pj_status_t status = pjsua_acc_get_config(_acc_id, tmp_pool, &acc_cfg);
        if (status != PJ_SUCCESS)
        {
            printf("Failed to get acc cfg");
        }
        
        acc_cfg.transport_id = [self isIPv6] ? transport_id_ipv6 : transport_id;
        acc_cfg.ipv6_media_use = [self isIPv6] ? PJSUA_IPV6_ENABLED : PJSUA_IPV6_DISABLED;
        
        acc_cfg.ice_cfg.enable_ice = [self isIPv6] ? YES : NO;
        acc_cfg.ice_cfg.enable_ice = YES;
        
        status = pjsua_acc_modify(_acc_id, &acc_cfg);
        if (status != PJ_SUCCESS)
        {
            printf("Failed to modify acc cfg");
        }
        
        pjsua_ip_change_param param;
        pjsua_ip_change_param_default(&param);
        
        pjsua_handle_ip_change(&param);
    }
}

- (void)reinvite
{
    NSString* serverStr = [Utils getServer];
    NSString* portStr = [Utils getPort];
    
    char* server = (char*)serverStr.UTF8String;
    char* port = (char*)portStr.UTF8String;
    
    char target_uri[MAX_SIP_REG_URI_LENGTH];
    sprintf(target_uri, "sip:%s:%s", server, port);
    
    pjsua_msg_data msg_data;
    pjsua_msg_data_init(&msg_data);
    msg_data.target_uri = pj_str(target_uri);
    
    pjsua_call_reinvite(active_call_id, PJSUA_CALL_UPDATE_TARGET | PJSUA_CALL_REINIT_MEDIA | PJSUA_CALL_UPDATE_CONTACT | PJSUA_CALL_UPDATE_VIA , &msg_data);
}

#pragma mark - Utils

- (void)handleRegistrationStateChangeWithRegInfo:(pjsua_reg_info *)info
{
    if (info->regc)
    {
        BOOL isRegistered = NO;
        
        if (info->cbparam)
        {
            switch (info->cbparam->code)
            {
                case 200:
                    isRegistered = YES;
                    break;
                default:
                    break;
            }
        }
        
        [listeners enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            id<XCPjsuaListener> listener = (id<XCPjsuaListener>)obj;
            if ([listener respondsToSelector:@selector(onRegState:)])
            {
                [listener onRegState:isRegistered];
            }
        }];
        
        if (isRegistered)
        {
            unsigned call_count = pjsua_call_get_count();
            if (call_count)
            {
                [self reinvite];
            }
        }
    }
}

- (void)handleCall:(int)callId state:(int)callState
{
    dispatch_async(dispatch_get_main_queue(), ^{
        CallState state = (CallState)callState;
        
        [listeners enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            id<XCPjsuaListener> listener = (id<XCPjsuaListener>)obj;
            if ([listener respondsToSelector:@selector(onCallState:callId:)])
            {
                [listener onCallState:state callId:callId];
            }
        }];
    });
}

- (BOOL)isIPv6
{
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    return [appDelegate isIPv6];
}

@end



#pragma mark - Pjsip callbacks

/* Callback called by the library when registration state has changed */
static void on_reg_state2(pjsua_acc_id acc_id, pjsua_reg_info *info)
{
    if (![NSThread isMainThread])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[XCPjsua sharedXCPjsua] handleRegistrationStateChangeWithRegInfo:info];
        });
    }
    else
    {
        [[XCPjsua sharedXCPjsua] handleRegistrationStateChangeWithRegInfo:info];
    }
}

/* Callback called by the library upon receiving incoming call */
static void on_incoming_call(pjsua_acc_id acc_id, pjsua_call_id call_id,
                             pjsip_rx_data *rdata)
{
    pjsua_call_info ci;
    pjsua_call_get_info(call_id, &ci);

    PJ_LOG(3,(THIS_FILE, "Incoming call from %.*s!!", (int)ci.remote_info.slen, ci.remote_info.ptr));

    [[XCPjsua sharedXCPjsua] handleCall:call_id state:CallStateIncoming];
}

/* Callback called by the library when call's state has changed */
static void on_call_state(pjsua_call_id call_id, pjsip_event *e)
{
    pjsua_call_info ci;
    pjsua_call_get_info(call_id, &ci);
    pjsip_inv_state callState = ci.state;
    
    PJ_LOG(3,(THIS_FILE, "Call %d state=%.*s", call_id, (int)ci.state_text.slen, ci.state_text.ptr));
    
    [[XCPjsua sharedXCPjsua] handleCall:call_id state:callState];
}

/* Callback called by the library when call's media state has changed */
static void on_call_media_state(pjsua_call_id call_id)
{
    pjsua_call_info ci;

    pjsua_call_get_info(call_id, &ci);

    if (ci.media_status == PJSUA_CALL_MEDIA_ACTIVE) {
        // When media is active, connect call to sound device.
        pjsua_conf_connect(ci.conf_slot, 0);
        pjsua_conf_connect(0, ci.conf_slot);
    }
}

static void on_transport_state(pjsip_transport *tp,
                               pjsip_transport_state state,
                               const pjsip_transport_state_info *info)
{
    printf("on_transport_state: %d", state);
}
